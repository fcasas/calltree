from sys import argv
import re

RE_FUNCTION_DECLARATION = re.compile(r"([\w*]+ )+\*?(?P<name>\w+)\(")
RE_NO_CALLER = re.compile(r"[^\W/#]")
RE_FUNCTION_CALL = re.compile(r"(?P<name>\w+)\(")
FUNCTION_TYPE_COLORS = {
    "base"   : "\033[0;1m*",
    "single" : "\033[0m>",
    "multi"  : "\033[0;32m~",
    "weak"   : "\033[0;35m·",
    "scc"    : "\033[0;33m+",
    "nondef" : "\033[0;37m-",
    }
COLOR_RESET = "\033[0m"

params = [arg for arg in argv[1:] if arg[0] == '-']
fnames = [arg for arg in argv[1:] if arg[0] != '-']

if len(fnames) == 0:
    print("usage: python3 %s [OPTION] {<input-file>}"%argv[0])
    print("")
    print("Available options:")
    print("  -x : Show non-declared functions.")
    exit(1)

# based on https://www.geeksforgeeks.org/strongly-connected-components/
def kosaraju_strongly_connected_components(vertices, edges):
    # Vertex name to index
    idx = dict([(s, i) for (i, s) in enumerate(vertices)])

    # Number of nodes
    n_nodes = len(idx)

    # Graph using indexes
    graph = dict([(idx[s], [idx[n] for n in ns if n in idx]) for (s, ns) in edges.items()])

    # Transposed graph
    graph_inv = dict([(i, []) for i in range(n_nodes)])
    for i in range(n_nodes):
        for j in graph[i]:
            graph_inv[j].append(i)

    # If nodes are visited
    visited = [False] * n_nodes

    # Stack of visited nodes
    stack = []

    # Mark nodes as visited using DFS in the inverted graph, and return all reached ones
    def dfs(i):
        connected = [i]
        visited[i] = True
        for j in graph_inv[i]:
            if not visited[j]:
                connected += dfs(j)
        return connected

    # Put nodes in the stack, in DFS
    def fill_stack_dfs(i):
        visited[i] = True
        for j in graph[i]:
            if not visited[j]:
                fill_stack_dfs(j)
        stack.append(i)

    # Put vertices in stack to get the visting order
    for i in range(n_nodes):
        if not visited[i]:
            fill_stack_dfs(i)

    # Mark all vertices as not visited, again
    visited = [False] * n_nodes

    # Final groups
    groups = []

    # Process all vertices in order defined by stack
    for i in stack[::-1]:
        if not visited[i]:
            group = dfs(i)
            if len(group) > 0:
                groups.append(group)

    # Back to names again
    return [[vertices[i] for i in g] for g in groups]

"""
- Declared functions with 0 parent callers are displayed alone, with all their callees.
- Declared functions with 1 parent caller are displayed inside the parent, with all their callees.
- Declared functions with more than 1 parent caller are named inside the parent, but only displayed
    later with all their callees.
- Non-declared functions are only named.

There is an exception:
- Functions that are part of strongly connected components (call loops) with at least one other
  function follow the same rule as functions with more than 1 parent.
"""

declarations = []
calls = {}
locations = {}
current_caller = None
recursive = set()

for fname in fnames:
    fo = open(fname, "r")

    # Build calls
    for i, line in enumerate(fo):
        match = RE_FUNCTION_DECLARATION.match(line)

        if match:
            current_caller = match['name']

            if current_caller not in declarations:
                declarations.append(current_caller)
                calls[current_caller] = []

            # Note that if the location is already stored, override. Usually the last one is the definition.
            if len(fnames) == 1:
                locations[current_caller] = ":"+str(i + 1)
            else:
                locations[current_caller] = fname+":"+str(i + 1)

        elif RE_NO_CALLER.match(line):
            current_caller = None

        elif current_caller:
            finds = RE_FUNCTION_CALL.findall(line)

            for callee in finds:
                if callee == current_caller:
                    recursive.add(callee)
                    continue
                if callee in calls[current_caller]: # note: may be faster
                    continue
                calls[current_caller].append(callee)

            # Try to find previously declared functions even if they are not called (may be fn. pointer)
            for name in declarations:
                if name == current_caller:
                    # todo: maybe recursive?
                    continue
                if name in calls[current_caller]: # note: may be faster
                    continue
                if re.search(r'\b' + name + r'\b', line):
                    calls[current_caller].append(name)
    fo.close()


# Find strongly connected components
sccs = kosaraju_strongly_connected_components(declarations, calls)

# Find number of parents for each declared function
n_parents = dict([(f, 0) for f in declarations])
for caller, callees in calls.items():
    for c in callees:
        if c in declarations: # note: may be faster
            n_parents[c] += 1

# Add fake parents to functions in scc with other functions
for group in sccs:
    for f in group:
        n_parents[f] += len(group) - 1

# Get scc index for each function
sccidx = {}
for gidx, group in enumerate(sccs):
    for f in group:
        sccidx[f] = gidx


function_type = {}
for func in declarations:

    # Functions without parents
    if n_parents[func] == 0:
        function_type[func] = "base"

    # Functions with a single parent
    if n_parents[func] == 1:
        function_type[func] = "single"

    # Multi-parent function
    if n_parents[func] > 1:
        # Multi-parent functions without callee to show aren't displayed alone
        has_callee = False
        for c in calls[func]:
            if c in declarations or "-x" in params:
                has_callee = True
                break
        function_type[func] = "multi" if has_callee else "weak"

    # Function that belongs to a larger SCC (overrides)
    if len(sccs[sccidx[func]]) > 1:
        function_type[func] = "scc"


# Print function with a given indentation
def print_function(name, indent):
    tags = ""
    if name in recursive:
        tags += " [rec]"
    if (name in function_type) and function_type[name] == "scc":
        tags += " [scc]"

    # Print name and location
    if name in declarations:
        color = FUNCTION_TYPE_COLORS[function_type[name]]
        print("    " * indent + f"{color} {name} ({locations[name]}){tags}{COLOR_RESET}")
    elif "-x" in params:
        color = FUNCTION_TYPE_COLORS["nondef"]
        print("    " * indent + f"{color} {name}{COLOR_RESET}")

    # Print callees
    if name in declarations and ((indent == 0) or (n_parents[name] == 1)):
        for callee in calls[name]:
            print_function(callee, indent + 1)

displayed = set()
# Display all functions without parents
for func in declarations:
    if function_type[func] == "base":
        print_function(func, 0)
        displayed.add(func)
        print("")

# Display remaining functions
for func in declarations:

    # Single parent functions are displayed inside their parents
    if function_type[func] == "single":
        continue

    # Functions without callee to show (and more than 1 parent) don't need to be displayed again
    if function_type[func] == "weak":
        continue

    # Functions already displayed are skipped
    if func in displayed:
        continue

    # Display function and all other functions in SCC together
    funcs = sccs[sccidx[func]]

    for func in funcs:
        print_function(func, 0)
        displayed.add(func)
    print("")



