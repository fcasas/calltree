# calltree

Quick and dirty script that heuristically finds function definitions and calls in `.c` files in order
to build a simplified call tree to have a somewhat easier time when diving in new codebases.

## Usage

```
python3 calltree.py [OPTION] {<input-file>}

Available options:
  -x : Show non-declared functions.
```

## Example output

![Example output.](example_output.png)
